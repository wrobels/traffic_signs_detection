package mgr.seb.trafficsigns;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import org.tensorflow.lite.Interpreter;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import static mgr.seb.trafficsigns.Configurator.*;

class TrafficSignsClassifier {

	private final Interpreter interpreter;

	private TrafficSignsClassifier(Interpreter interpreter) {
		this.interpreter = interpreter;
	}

	static TrafficSignsClassifier classifier(AssetManager assetManager, String modelPath) throws IOException {
		ByteBuffer byteBuffer = loadModelFile(assetManager, modelPath);
		Interpreter interpreter = new Interpreter(byteBuffer);
		return new TrafficSignsClassifier(interpreter);
	}

	private static ByteBuffer loadModelFile(AssetManager assetManager, String modelPath) throws IOException {
		AssetFileDescriptor fileDescriptor = assetManager.openFd(modelPath);
		FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
		FileChannel fileChannel = inputStream.getChannel();
		long startOffset = fileDescriptor.getStartOffset();
		long declaredLength = fileDescriptor.getDeclaredLength();
		return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
	}


	List<PotentialSign> recognizeImage(Bitmap bitmap) {
		ByteBuffer byteBuffer = convertBitmapToByteBuffer(bitmap);
		float[][] result = new float[1][Configurator.OUTPUT_LABELS.size()];
		interpreter.run(byteBuffer, result);
		return getSortedResult(result);
	}

	private ByteBuffer convertBitmapToByteBuffer(Bitmap bitmap) {
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(MODEL_INPUT_SIZE);
		byteBuffer.order(ByteOrder.nativeOrder());
		int[] intValues = new int[INPUT_IMG_SIZE_WIDTH * INPUT_IMG_SIZE_HEIGHT];
		bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
		int pixel = 0;
		for (int i = 0; i < INPUT_IMG_SIZE_WIDTH; ++i) {
			for (int j = 0; j < INPUT_IMG_SIZE_HEIGHT; ++j) {
				final int val = intValues[pixel++];
				byteBuffer.putFloat((((val >> 16) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
				byteBuffer.putFloat((((val >> 8) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
				byteBuffer.putFloat((((val) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
			}
		}
		return byteBuffer;
	}

	private List<PotentialSign> getSortedResult(float[][] resultsArray) {
		PriorityQueue<PotentialSign> sortedResults = new PriorityQueue<>(
				MAX_CLASSIFICATION_RESULTS,
				(lhs, rhs) -> Float.compare(rhs.confidence, lhs.confidence)
		);

		for (int i = 0; i < Configurator.OUTPUT_LABELS.size(); ++i) {
			float confidence = resultsArray[0][i];
			if (confidence > CLASSIFICATION_THRESHOLD) {
				Configurator.OUTPUT_LABELS.size();
				sortedResults.add(new PotentialSign(Configurator.OUTPUT_LABELS.get(i), confidence));
			}
		}

		return new ArrayList<>(sortedResults);
	}

}
