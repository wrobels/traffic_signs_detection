package mgr.seb.trafficsigns;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.*;
import org.opencv.android.*;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RecognizeActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static Handler signHandler = new Handler();

    private Runnable recognitionThread, signPreviewThread;

    private TrafficSignsClassifier trafficSignsClassifier;

    private static final String TAG = RecognizeActivity.class.getName();


    private CameraBridgeViewBase mOpenCvCameraView;
    private Switch fullScreenRecognitionSwitch, recognitionSwitch;
    private ImageView recognizedSignPreview, foundSignPreview;

    private Mat inputImage;
    private Mat inputImageTransposed;
    private Mat inputImageResized;
    private Mat grayImage;

    private SignsDetector signsDetector;

    private Bitmap signToShow;

    private ArrayList<Bitmap> foundCircularSignsList;
    private ArrayList<Bitmap> foundRectangularSignsList;
    private List<PotentialSign> recognitionList;
    private boolean recognizeModeEnabled = false;
    private boolean fullScreenRecognition = true;

    private int methodIndex = 1;

    static {
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "OpenCV not loaded");
        } else {
            Log.d(TAG, "OpenCV loaded");
        }
    }


    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    Log.i(TAG, "OpenCV loaded successfully");
                    //	if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    mOpenCvCameraView.setMaxFrameSize(720, 720);
                    //	}
                    mOpenCvCameraView.enableView();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "OpenCV not loaded");
        } else {
            Log.d(TAG, "OpenCV loaded");
        }

        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_recognizer);

        methodIndex = Objects.requireNonNull(getIntent().getIntExtra("recognitionMethod", 1));

        initializeViewComponents();

        foundCircularSignsList = new ArrayList<>();
        foundRectangularSignsList = new ArrayList<>();
        recognitionList = new ArrayList<>();

        signsDetector = new SignsDetector(getApplicationContext(), foundCircularSignsList, foundRectangularSignsList);

        loadSignClassifier();

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        fullScreenRecognitionSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> fullScreenRecognition = isChecked);
        recognitionSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> recognizeModeEnabled = isChecked);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface
                    .SUCCESS);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null) mOpenCvCameraView.disableView();

        signHandler.removeCallbacks(recognitionThread);
        signHandler.removeCallbacks(signPreviewThread);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null) mOpenCvCameraView.disableView();

        signHandler.removeCallbacks(recognitionThread);
        signHandler.removeCallbacks(signPreviewThread);
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mOpenCvCameraView != null) mOpenCvCameraView.disableView();

        signHandler.removeCallbacks(recognitionThread);
        signHandler.removeCallbacks(signPreviewThread);
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        inputImage = new Mat(width, height, CvType.CV_8U, new Scalar(4));
        inputImageResized = new Mat(height, width, CvType.CV_8U, new Scalar(4));
        inputImageTransposed = new Mat(height, width, CvType.CV_8U, new Scalar(4));
        grayImage = new Mat(height, width, CvType.CV_8U, new Scalar(4));
    }

    @Override
    public void onCameraViewStopped() {
        inputImage.release();
        inputImageTransposed.release();
        inputImageResized.release();
        grayImage.release();

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        foundRectangularSignsList.clear();
        foundCircularSignsList.clear();

        inputImage = inputFrame.rgba();

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            rotateInputImage(inputImage);
        }

        switchRecognitionMethod(methodIndex);

        startThreads();

        return inputImage;

    }

    private void initializeViewComponents() {
        mOpenCvCameraView = (JavaCameraView) findViewById(R.id.camera_viewer);
        fullScreenRecognitionSwitch = findViewById(R.id.full_screen_recognition_switch);
        recognizedSignPreview = findViewById(R.id.recognized_sign_preview);
        foundSignPreview = findViewById(R.id.found_sign_preview);
        recognitionSwitch = findViewById(R.id.recognition_switch);
    }

    private void loadSignClassifier() {
        try {
            trafficSignsClassifier = TrafficSignsClassifier.classifier(getAssets(), Configurator.MODEL_FILENAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void rotateInputImage(Mat inputImage) {

        // Rotate inputImage 90 degrees
        Core.transpose(inputImage, inputImageTransposed);
        Imgproc.resize(inputImageTransposed, inputImageResized, inputImageResized.size(), 0, 0, 0);
        Core.flip(inputImageResized, inputImage, 1);

    }

    private void startThreads() {

        signPreviewThread = () -> {
            try {
                switch (methodIndex) {
                    case 1:
                        if (foundCircularSignsList.size() > 0) {

                            signToShow = foundCircularSignsList.get(0);
                            foundSignPreview.setImageBitmap(null);
                            foundSignPreview.setImageBitmap(signToShow);
                        }
                        break;
                    case 2:
                        if (foundRectangularSignsList.size() > 0) {

                            signToShow = foundRectangularSignsList.get(0);
                            foundSignPreview.setImageBitmap(null);
                            foundSignPreview.setImageBitmap(signToShow);
                        }
                        break;
                    case 3:
                        if (foundRectangularSignsList.size() > 0) {

                            signToShow = foundRectangularSignsList.get(0);
                            foundSignPreview.setImageBitmap(null);
                            foundSignPreview.setImageBitmap(signToShow);
                        }
                        break;
                    default:
                        return;
                }

            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        };

        signHandler.post(signPreviewThread);

        if (recognizeModeEnabled) {

            recognitionThread = () -> {
                if (signToShow != null) {
                    recognitionList = trafficSignsClassifier.recognizeImage(signToShow);
                    recognizedSignPreview.setImageDrawable(null);
                    //	Toast.makeText(ActivityRecognizer.this, recognitionList.get(0).toString(), Toast.LENGTH_SHORT).show();
                    int resID = getResources().getIdentifier("sign_" + recognitionList.get(0), "drawable", getPackageName());
                    if (resID != 0x0) {
                        //	recognizedSignPreview.setImageDrawable(null);
                        recognizedSignPreview.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), resID));
                    }
                    Log.d("														ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss", recognitionList.get(0).toString());

                }
            };

            signHandler.post(recognitionThread);
        }

    }

    private void switchRecognitionMethod(int chosenMethodIndex) {
        switch (chosenMethodIndex) {
            case 1:
                signsDetector.detectRoundSigns(inputImage, grayImage, fullScreenRecognition);
                break;
            case 2:
                signsDetector.detectRectangularSigns(inputImage, grayImage, fullScreenRecognition);
                break;
            case 3:
                signsDetector.detectTriangularSigns(inputImage, grayImage, fullScreenRecognition);
                break;
            case 4:
                signsDetector.detectRectangularSigns(inputImage, grayImage, fullScreenRecognition);
                grayImage.release();
                signsDetector.detectRoundSigns(inputImage, grayImage, fullScreenRecognition);
                grayImage.release();
                signsDetector.detectTriangularSigns(inputImage, grayImage, fullScreenRecognition);
                break;
            default:
                signsDetector.detectRoundSigns(inputImage, grayImage, fullScreenRecognition);
                break;
        }
    }
}