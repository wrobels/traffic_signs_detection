package mgr.seb.trafficsigns;

import android.Manifest;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MenuActivity extends AppCompatActivity {

	private Button startRecognitionButton;
	private RadioGroup signsOptionsGroup;
	private int recognitionMethod = 1;

	private final int CAMERA_PERMISSION_CODE = 333;
	private final int SHARING_PERMISSIONS_CODE = 432;
	private String[] SHARING_PERMISSIONS = {
			Manifest.permission.WRITE_EXTERNAL_STORAGE,
			Manifest.permission.READ_EXTERNAL_STORAGE
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);

		ActivityCompat.requestPermissions(MenuActivity.this, new String[] {Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE); //tak naprawdze powinno to zostaż przeniesione do RecognizeActivity

		startRecognitionButton = findViewById(R.id.start_recognition_button);
		signsOptionsGroup = findViewById(R.id.sign_types_radio_group);

		startRecognitionButton.setOnClickListener(v -> {

			Intent intent = new Intent(MenuActivity.this, RecognizeActivity.class);
			intent.putExtra("recognitionMethod", recognitionMethod);
			startActivity(intent);

			Toast.makeText(getApplicationContext(), "Recognition started", Toast.LENGTH_LONG).show();
		});

		signsOptionsGroup.setOnCheckedChangeListener((group, checkedId) -> {
			switch (checkedId) {
				case R.id.speed_limit_sign_option:
					recognitionMethod = 1;
					break;
				case R.id.info_sign_option:
					recognitionMethod = 2;
					break;
				case R.id.warning_signs_option:
					recognitionMethod = 3;
					break;
				case R.id.all_signs_option:
					recognitionMethod = 4;
				default:
					recognitionMethod = 1;
					break;
			}

			Toast.makeText(getApplicationContext(), "recognitionMethod boolean = " + recognitionMethod, Toast.LENGTH_LONG).show();
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}
	@Override
	protected void onStart(){
		super.onStart();
	}
}
