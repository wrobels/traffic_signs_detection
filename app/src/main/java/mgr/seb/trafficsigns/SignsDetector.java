package mgr.seb.trafficsigns;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import org.opencv.android.Utils;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class SignsDetector {

    public static final int IMAGE_WIDTH = 224;
    public static final int IMAGE_HEIGHT = 224;

    private ArrayList<MatOfPoint> contours = new ArrayList<>();
    private Mat hierarchy = new Mat();
    private Mat circles = new Mat();

    private Context mContext;
    private int kernelSize = 3;//20 nawet git
    private int blurSize = 11;//11 bylo

    private ArrayList<Bitmap> foundCircularSignsList;
    private ArrayList<Bitmap> foundRectangularSignsList;

    int imageHeight = 0;
    int imageWidth = 0;
    int startWidth = 0;


    private Mat subInputImage, kernel;

//	private ArrayList<Moments> momentsListAll = new ArrayList<>();
//	private ArrayList<Moments> momentsListFiltered = new ArrayList<>();
//	private ArrayList<Point> massCenter = new ArrayList<>();

    SignsDetector(Context context, ArrayList<Bitmap> foundCircularSignsList, ArrayList<Bitmap> foundRectangularSignsList) {
        this.mContext = context;
        this.foundCircularSignsList = foundCircularSignsList;
        this.foundRectangularSignsList = foundRectangularSignsList;
    }

    private void prepareImageForRecognition(Mat inputImage, Mat grayImage) {

        kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(9, 9));
        Mat blurredGrayImage = new Mat();
        Imgproc.medianBlur(inputImage, grayImage, 5);
        Imgproc.cvtColor(grayImage, grayImage, Imgproc.COLOR_RGB2GRAY);
        Imgproc.equalizeHist(grayImage, grayImage);
        Imgproc.morphologyEx(grayImage, grayImage, Imgproc.MORPH_CLOSE, kernel);
        Imgproc.blur(grayImage, blurredGrayImage, new Size(blurSize, blurSize));
        Core.subtract(grayImage, blurredGrayImage, grayImage);
		/*
		kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(kernelSize, kernelSize));
		Imgproc.dilate(grayImage, grayImage, kernel);
		Imgproc.threshold(grayImage, grayImage, 10, 255, Imgproc.THRESH_BINARY);
		*/
    }

    void detectRoundSigns(Mat inputImage, Mat grayImage, boolean fullScreenRecognition) {

        imageHeight = inputImage.height();
        imageWidth = inputImage.width();


        if (!fullScreenRecognition) {
            startWidth = imageWidth - imageWidth / 3;
            subInputImage = inputImage.submat(0, imageHeight, startWidth, imageWidth);
        } else {
            subInputImage = inputImage;
        }

        prepareImageForRecognition(subInputImage, grayImage);

        Imgproc.HoughCircles(grayImage, circles, Imgproc.HOUGH_GRADIENT, 1.0, 50.0, 50, 50, 30, 120); //minDist = 320 byl calkiem ok, maxRadius = 120 tez byl ok

        if (!circles.empty()) {
            markRoundSigns(inputImage, circles, startWidth);
        }
    }//detectRoundSigns()

    void detectRectangularSigns(Mat inputImage, Mat grayImage, boolean fullScreenRecognition) {
        imageHeight = inputImage.height();
        imageWidth = inputImage.width();


        if (!fullScreenRecognition) {
            startWidth = imageWidth - imageWidth / 3;
            subInputImage = inputImage.submat(0, imageHeight, startWidth, imageWidth);
        } else {
            subInputImage = inputImage;
        }

        prepareImageForRecognition(subInputImage, grayImage);

        kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(kernelSize, kernelSize));
        Imgproc.dilate(grayImage, grayImage, kernel);
        Imgproc.threshold(grayImage, grayImage, 10, 255, Imgproc.THRESH_BINARY);
        Imgproc.findContours(grayImage, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE); // ApproxSimple powoduje znalezienie tylko 2 punktow na odcinku i laczenie skrajnych zamiast np 1000 punktow na calej dlugosci odcinka
        //Imgproc.findContours(grayImage, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE); // ApproxSimple powoduje znalezienie tylko 2 punktow na odcinku i laczenie skrajnych zamiast np 1000 punktow na calej dlugosci odcinka

        ArrayList<MatOfPoint> foundRectangles = findRectangularShapes(contours);
        ArrayList<Rect> boundedRectanglesToDraw = new ArrayList<>();


        if (!foundRectangles.isEmpty()) {
/*

			// znajduje srodki ciezkosci wszystkich prostokatow. jezeli sie pokrywaja to aby nie zaznaczac dwoch jeden nad drugim usuwa jeden z nich z listy
			for (MatOfPoint foundRectangle : foundRectangles) {
				momentsListAll.add(Imgproc.moments(foundRectangle));
			}

			for (int i = 0; i < momentsListAll.size(); i++) {
				//add 1e-5 to avoid division by zero
				massCenter.add(new Point(momentsListAll.get(i).m10 / (momentsListAll.get(i).m00 + 1e-5), momentsListAll.get(i).m01 / (momentsListAll.get(i).m00 + 1e-5)));
			}

			for (Point pt : massCenter) {
				Imgproc.circle(inputImage, pt, 4, new Scalar(255, 0, 0), -1);
			}

			Point tmpMassCenter;

			for (int i = 0; i < massCenter.size()-1; i++) {

				tmpMassCenter = massCenter.get(i);

				for (int j = 0; j < massCenter.size()-1; j++) {
					if (j != i && tmpMassCenter == massCenter.get(j))
						foundRectangles.remove(i);
				}
			}
*/

            for (MatOfPoint rectangle : foundRectangles) {
                Rect rectangleToDraw = Imgproc.boundingRect(rectangle);

                if (rectangleToDraw.height >= 50 && rectangleToDraw.width >= 50) { //tutaj do listy dodajemy wszystkie prostokaty wieksze niz 50x50 wiec kilka znakow na raz mozna wyswietlac

                    boundedRectanglesToDraw.add(rectangleToDraw);

                }
            }
        }


        for (Rect rectangle : boundedRectanglesToDraw) { // dziala ladnie, oznacza prostokaty bez tych malych gowien

            Imgproc.rectangle(inputImage, new Point(rectangle.x + startWidth, rectangle.y), new Point(rectangle.x + rectangle.width + startWidth, rectangle.y + rectangle.height), new Scalar(0, 255, 255), 3);

            //if (rectangle.x > 5 && rectangle.x + 5 <= inputImage.rows() && rectangle.y > 5 && rectangle.y + 5 <= inputImage.cols()) {
            foundRectangularSignsList.add(rescaleImageBeforeRecognition(createRectangularSignBitmap(subInputImage, rectangle)));
            //}
        }
        contours.clear();
    }//detectRectangularSigns()

    void detectTriangularSigns(Mat inputImage, Mat grayImage, boolean fullScreenRecognition) {

        imageHeight = inputImage.height();
        imageWidth = inputImage.width();

		if (!fullScreenRecognition) {
			startWidth = imageWidth - imageWidth / 3;
			subInputImage = inputImage.submat(0, imageHeight, startWidth, imageWidth);
		} else {
			subInputImage = inputImage;
		}

        prepareImageForRecognition(subInputImage, grayImage);

        kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(kernelSize, kernelSize));
        Imgproc.dilate(grayImage, grayImage, kernel);
        Imgproc.threshold(grayImage, grayImage, 10, 255, Imgproc.THRESH_BINARY);

        Imgproc.findContours(grayImage, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE); // ApproxSimple powoduje znalezienie tylko 2 punktow na odcinku i laczenie skrajnych zamiast np 1000 punktow na calej dlugosci odcinka

        //Imgproc.findContours(grayImage, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE); // ApproxSimple powoduje znalezienie tylko 2 punktow na odcinku i laczenie skrajnych zamiast np 1000 punktow na calej dlugosci odcinka

        ArrayList<MatOfPoint> foundTriangles = findTriangularShapes(contours);


        for (int i = 0; i < foundTriangles.size(); i++) {

            Rect rect = Imgproc.boundingRect(foundTriangles.get(i));

            foundRectangularSignsList.add(rescaleImageBeforeRecognition(createRectangularSignBitmap(inputImage, rect)));

            Imgproc.drawContours(inputImage, foundTriangles, i, new Scalar(0, 0, 255), 10);
        }

        contours.clear();
    }//detectTriangularSigns()

    private static Bitmap createRectangularSignBitmap(Mat src, Rect rectangle) {

        Mat rectangleToSave;
        Bitmap rectangleToSaveBitmap;

        rectangleToSave = src.submat(rectangle.y, rectangle.y + rectangle.height, rectangle.x, rectangle.x + rectangle.width);

        rectangleToSaveBitmap = Bitmap.createBitmap(rectangleToSave.cols(), rectangleToSave.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(rectangleToSave, rectangleToSaveBitmap);

        return rectangleToSaveBitmap;

    }

    private static ArrayList<MatOfPoint> findRectangularShapes(List<MatOfPoint> contours) {

        ArrayList<MatOfPoint> rectanglesList = new ArrayList<>();

        for (MatOfPoint contour : contours) {

            MatOfPoint2f shape = new MatOfPoint2f(contour.toArray());

            MatOfPoint2f approxShape = new MatOfPoint2f();
            Imgproc.approxPolyDP(shape, approxShape, 0.02 * Imgproc.arcLength(shape, true), true);

            int numberOfVertices = (int) approxShape.total();

            double contourArea = Imgproc.contourArea(contour);


            if (Math.abs(contourArea) < 200) {
                continue;
            }

            if (numberOfVertices >= 4 && numberOfVertices <= 6) {
                List<Double> cosine = new ArrayList<>();

                for (int j = 2; j < numberOfVertices + 1; j++) {
                    cosine.add(calculateAngleCosine(approxShape.toArray()[j % numberOfVertices], approxShape.toArray()[j - 2], approxShape.toArray()[j - 1]));
                }

                Collections.sort(cosine);

                double minCosine = cosine.get(0);
                double maxCosine = cosine.get(cosine.size() - 1);

                if (numberOfVertices == 4 && minCosine >= -0.2 && maxCosine <= 0.3) {

                    rectanglesList.add(contour);

                }
            }

        }

        return rectanglesList;
    } //markRectangularSigns()

    private static ArrayList<MatOfPoint> findTriangularShapes(List<MatOfPoint> contours) {

        ArrayList<MatOfPoint> trianglesList = new ArrayList<>();
        for (MatOfPoint contour : contours) {

            MatOfPoint2f shape = new MatOfPoint2f(contour.toArray());

            MatOfPoint2f approxShape = new MatOfPoint2f();
            Imgproc.approxPolyDP(shape, approxShape, 0.02 * Imgproc.arcLength(shape, true), true);

            int numberOfVertices = (int) approxShape.total();

            double contourArea = Imgproc.contourArea(contour);


            if (Math.abs(contourArea) < 100) { // bylo 200
                continue;
            }

            if (numberOfVertices >= 2 && numberOfVertices <= 4) {
                List<Double> cosine = new ArrayList<>();

                for (int j = 2; j < numberOfVertices + 1; j++) {
                    cosine.add(calculateAngleCosine(approxShape.toArray()[j % numberOfVertices], approxShape.toArray()[j - 2], approxShape.toArray()[j - 1]));
                }

                Collections.sort(cosine);

                double minCosine = cosine.get(0);
                double maxCosine = cosine.get(cosine.size() - 1);

                if (numberOfVertices == 3 && minCosine >= 0.4 && maxCosine <= 0.60) {

                    trianglesList.add(contour);

                }
            }

        }

        return trianglesList;
    } //markTriangularSigns()

    private void markRoundSigns(Mat inputImage, Mat circles, int additionalWidth) {
        Bitmap roundSignToAddBitmap;
        //	foundCircularSignsList.clear();
        for (int j = 0; j < circles.cols(); j++) {
            double[] circ = circles.get(0, j);
            if (circ == null) break;

            Point circleCenter = new Point(Math.round(circ[0]) + additionalWidth, Math.round(circ[1]));
            int r = (int) Math.round(circ[2]);
            //Imgproc.rectangle(inputImage, new Point(pt.x - r - 5, pt.y - r - 5), new Point(pt.x + r + 5, pt.y + r + 5), new Scalar(255, 0, 255), 3);

            if (circleCenter.x - r > 5 && circleCenter.x + r + 5 <= inputImage.rows() && circleCenter.y - r > 5 && circleCenter.y + r + 5 <= inputImage.cols()) {

                roundSignToAddBitmap = createRoundSignBitmap(inputImage, circleCenter, r);
                foundCircularSignsList.add(rescaleImageBeforeRecognition(roundSignToAddBitmap));
                //saveCroppedRoundSign(rescaleImageBeforeRecognition(createRoundSignBitmap(inputImage, circleCenter, r)), j);
            }

            Imgproc.circle(inputImage, circleCenter, r, new Scalar(0, 255, 0), 3);

        }
        circles.release();

    } //markRoundSigns()

    private static double calculateAngleCosine(Point point1, Point point2, Point point0) {

        double d1x = point1.x - point0.x;
        double d1y = point1.y - point0.y;
        double d2x = point2.x - point0.x;
        double d2y = point2.y - point0.y;

        return (((d1x * d2x) + (d1y * d2y)) / Math.sqrt((d1x * d1x + d1y * d1y) * (d2x * d2x + d2y * d2y)) + 1e-10);
    }//calculateAngleCosine()

    private Bitmap rescaleImageBeforeRecognition(Bitmap bitmap) {

        Paint paint = new Paint();
        Bitmap finalBitmap = Bitmap.createScaledBitmap(
                bitmap,
                IMAGE_WIDTH,
                IMAGE_HEIGHT,
                false);
        Canvas canvas = new Canvas(finalBitmap);
        canvas.drawBitmap(finalBitmap, 0, 0, paint);
        return finalBitmap;
    } //rescaleImageBeforeRecognition()

    private static Bitmap createRoundSignBitmap(Mat src, Point circleCenter, int r) {

        Mat circleToSave;
        Bitmap circleToSaveBitmap;

        //	if (circleCenter.x - r > 5 && circleCenter.x + r + 5 <= src.rows() && circleCenter.y - r > 5 && circleCenter.y + r + 5 <= src.cols()) {
        /////////////// ROWS = Y, COLS = X !!!!!  /////////////////////
        circleToSave = src.submat((int) circleCenter.y - r - 5, (int) circleCenter.y + r + 5, (int) circleCenter.x - r - 5, (int) circleCenter.x + r + 5);
        //} else
        //circleToSave = src.submat(0, (int) circleCenter.x + r + 5, 0, (int) circleCenter.y + r + 5);

        circleToSaveBitmap = Bitmap.createBitmap(circleToSave.cols(), circleToSave.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(circleToSave, circleToSaveBitmap);

        return circleToSaveBitmap;

    }//createRoundSignBitmap()

}
