package mgr.seb.trafficsigns;

public class PotentialSign {
	private final String title;
	final float confidence;

	PotentialSign(String title, float confidence) {
		this.title = title;
		this.confidence = confidence;
	}

	@Override
	public String toString() {
		return title;
	}

}
